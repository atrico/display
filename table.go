package display

import (
	intEx "gitlab.com/atrico/core/v2/numbers/int"
	"gitlab.com/atrico/display/v2/common"
	"gitlab.com/atrico/display/v2/tile"
	"gitlab.com/atrico/display/v2/xy"
	"sort"
)

type TableBuilder interface {
	// Config
	WithHorizontalSeparator(separator rune) TableBuilder
	WithVerticalSeparator(separator rune) TableBuilder
	SetCell(x, y int, content any) TableBuilder
	SetCellByPosition(pos xy.Position, content any) TableBuilder
	AppendRow(content ...any) TableBuilder
	// tile.Renderable
	Build() tile.Renderable
}

// Content that spans columns
// span = no of colums - TODO -1 for all?
type Span struct {
	Content any
	Span    int
}

func NewTableBuilder() TableBuilder {
	return &table{cells: make(map[xy.Position]tile.Renderable)}
}

// ----------------------------------------------------------------------------------------------------------------------------
// Implementation
// ----------------------------------------------------------------------------------------------------------------------------

type spanDetail struct {
	y            int
	startX, endX int
	content      tile.Renderable
}

func (s spanDetail) isFullRow() bool {
	return s.endX == -1
}

type table struct {
	cells      map[xy.Position]tile.Renderable
	horizontal *rune
	vertical   *rune
	spans      []spanDetail
}

func (t *table) WithHorizontalSeparator(separator rune) TableBuilder {
	t.horizontal = &separator
	return t
}

func (t *table) WithVerticalSeparator(separator rune) TableBuilder {
	t.vertical = &separator
	return t
}

func (t *table) SetCell(x, y int, content any) TableBuilder {
	return t.SetCellByPosition(xy.NewPosition(x, y), content)
}

func (t *table) SetCellByPosition(pos xy.Position, content any) TableBuilder {
	if span, ok := content.(Span); ok {
		// Add a space in first cell to ensure column is not removed if otherwise empty
		t.cells[pos] = tile.NewRenderable(" ")
		// Add to list for dealing with at render time
		t.spans = append(t.spans, spanDetail{
			y:       pos.Y(),
			startX:  pos.X(),
			endX:    pos.X() + span.Span,
			content: tile.NewRenderable(span.Content),
		})
	} else {
		t.cells[pos] = tile.NewRenderable(content)
	}
	return t
}

func (t *table) AppendRow(content ...any) TableBuilder {
	// Find current extent
	y := t.countRowsAndColumns().Y()
	spanOff := 0
	for x, cell := range content {
		t.SetCell(x+spanOff, y, cell)
		if span, ok := cell.(Span); ok {
			spanOff += span.Span - 1
		}
	}
	return t
}

func (t *table) Build() tile.Renderable {
	return t
}

// Renderable
func (t *table) Render(rules ...tile.RenderRule) tile.Tile {
	cvs := NewCanvas()
	cellz, rows, cols := t.expandCells()
	cellOrigins := make(map[xy.Position]xy.Position)
	// Create horizontal separator
	var horizontal tile.Tile
	if t.horizontal != nil {
		width := 0
		for _, colWidth := range cols {
			width += colWidth
		}
		if t.vertical != nil {
			width += len(cols) - 1
		}
		horizontal = tile.NewTile(NewHorizontalLine(*t.horizontal, width))
	}
	yPos := 0
	for y := range cellz {
		xPos := 0
		// Separator?
		if y > 0 && t.horizontal != nil {
			cvs.WriteAt(xy.NewPosition(0, yPos), horizontal)
			yPos++
		}
		height := 0
		for x := range cellz[y] {
			// Separator?
			if x > 0 && t.vertical != nil {
				cvs.WriteAt(xy.NewPosition(xPos, yPos), NewVerticalLine(*t.vertical, rows[y]))
				xPos++
			}
			// Store cell pos
			cellOrigins[xy.NewPosition(x, y)] = xy.NewPosition(xPos, yPos)
			cell := tile.NewSizedTile(xy.NewSize(max(cols[x], cellz[y][x].Size().X()), rows[y]), cellz[y][x]).Render()
			cvs.WriteAt(xy.NewPosition(xPos, yPos), cell)
			xPos = xPos + cell.Size().X()
			height = max(height, cell.Size().Y())
		}
		yPos = yPos + height
	}
	// Add spans
	for _, span := range t.spans {
		widthOfSpan, _ := t.calculateSpanWidth(span, cols)
		content := tile.NewSizedTile(xy.NewSize(widthOfSpan, rows[span.y]), span.content)
		cvs.OverwriteAt(cellOrigins[xy.NewPosition(span.startX, span.y)], content)
	}
	return cvs.Render(rules...)
}

// ----------------------------------------------------------------------------------------------------------------------------
// internal
// ----------------------------------------------------------------------------------------------------------------------------

// Cells in form [row(y)][col(x)]
// Rows/cols are slices width/height of each
func (t *table) expandCells() (cells [][]tile.Tile, rows, cols []int) {
	size := t.countRowsAndColumns()
	// Update size based on spans
	for _, span := range t.spans {
		if !span.isFullRow() && size.X() < span.startX+span.endX-1 {
			size = xy.NewSize(span.startX+span.endX-1, size.Y())
		}
	}
	cells = make([][]tile.Tile, size.Y())
	rows = make([]int, size.Y())
	cols = make([]int, size.X())
	for y := range cells {
		cells[y] = make([]tile.Tile, size.X())
		for x := range cells[y] {
			if elem, ok := t.cells[xy.NewPosition(x, y)]; ok {
				cells[y][x] = elem.Render()
			} else {
				cells[y][x] = tile.NewTile("")
			}
			rows[y] = common.MaxInt(rows[y], cells[y][x].Size().Y())
			cols[x] = common.MaxInt(cols[x], cells[y][x].Size().X())
		}
	}
	// Calculate spans (end column order)
	sort.Slice(t.spans, func(i, j int) bool {
		correct := func(val int) int {
			if val == -1 {
				return intEx.MaxValue
			}
			return val
		}
		return correct(t.spans[i].endX) < correct(t.spans[j].endX)
	})
	for _, span := range t.spans {
		content := span.content.Render()
		// Update height
		if content.Size().Y() > rows[span.y] {
			rows[span.y] = content.Size().Y()
		}
		// Update width (of last column in span) if needed
		widthOfSpan, lastCol := t.calculateSpanWidth(span, cols)
		if content.Size().X() > widthOfSpan {
			cols[lastCol-1] += content.Size().X() - widthOfSpan
		}
	}
	return cells, rows, cols
}

func (t *table) calculateSpanWidth(span spanDetail, cols []int) (width, lastCol int) {
	lastCol = span.endX
	if span.isFullRow() {
		lastCol = len(cols)
	}
	for _, wid := range cols[span.startX:lastCol] {
		width += wid
	}
	if t.vertical != nil {
		width += lastCol - span.startX - 1
	}
	return
}

func (t *table) countRowsAndColumns() xy.Size {
	pos := xy.NewPosition(-1, -1)
	for k, _ := range t.cells {
		pos = xy.UpperPosition(pos, k)
	}
	return xy.NewSize(pos.X()+1, pos.Y()+1)
}
