package unit_tests

import (
	"fmt"
	"gitlab.com/atrico/console/ansi"
	rules2 "gitlab.com/atrico/display/v2/rules"
	"testing"

	"gitlab.com/atrico/display/v2/tile"

	"gitlab.com/atrico/display/v2"
	"gitlab.com/atrico/display/v2/xy"
)

func Test_Table_Empty(t *testing.T) {
	// Arrange

	// Act
	table := display.NewTableBuilder().
		Build()

	// Assert
	assertRenderable(t, table)
}

func Test_Table_SetCells(t *testing.T) {
	// Arrange

	// Act
	table := display.NewTableBuilder().
		SetCell(0, 0, "a").
		SetCell(1, 1, "b").
		SetCell(2, 2, "c").
		Build()

	// Assert
	assertRenderable(t, table,
		"a",
		" b",
		"  c")
}

func Test_Table_SetCellsSpan(t *testing.T) {
	// Arrange

	// Act
	table := display.NewTableBuilder().
		SetCell(0, 0, "a").
		SetCell(1, 1, display.Span{Content: "bb", Span: 2}).
		SetCell(2, 2, "c").
		Build()

	// Assert
	assertRenderable(t, table,
		"a",
		" bb",
		"  c")
}

func Test_Table_SetCells_Colors(t *testing.T) {
	// Arrange
	attr := ansi.NewAttributes(randomColour(), randomColour())

	// Act
	table := display.NewTableBuilder().
		SetCell(0, 0, "a").
		SetCell(1, 1, attr.ApplyWithResetTo("b")).
		SetCell(2, 2, "c").
		Build()

	// Assert
	assertRenderable(t, table,
		"a",
		" "+attr.ApplyWithResetTo("b"),
		"  c")
}

func Test_Table_SetCells_SpanColors(t *testing.T) {
	// Arrange
	attr := ansi.NewAttributes(randomColour(), randomColour())
	attr2 := ansi.NewAttributes(randomColour(), randomColour())
	attrInit := attr.SetThis()
	attrDelta := attr.CreateDeltaTo(attr2)
	attrEnd := attr2.ResetThis()

	// Act
	table := display.NewTableBuilder().
		SetCell(0, 0, "a").
		SetCell(1, 1, display.Span{Content: attr.ApplyWithResetTo("b") + attr2.ApplyWithResetTo("b"), Span: 2}).
		SetCell(2, 2, "c").
		Build()

	// Assert
	assertRenderable(t, table,
		"a",
		" "+attrInit.GetCodeString()+"b"+attrDelta.GetCodeString()+"b"+attrEnd.GetCodeString(),
		"  c")
}

func Test_Table_SetCellsDifferentWidths(t *testing.T) {
	// Arrange

	// Act
	table := display.NewTableBuilder().
		SetCell(0, 0, "one").SetCell(1, 0, "-").
		SetCell(0, 1, "2").SetCell(1, 1, "-").
		SetCell(0, 2, "three").SetCell(1, 2, "-").
		Build()

	// Assert
	assertRenderable(t, table,
		"one  -",
		"2    -",
		"three-")
}

func Test_Table_SetCellsDifferentHeights(t *testing.T) {
	// Arrange

	// Act
	table := display.NewTableBuilder().
		SetCell(0, 0, []string{"2", "2"}).SetCell(0, 1, "-").
		SetCell(1, 0, "1").SetCell(1, 1, "-").
		SetCell(2, 0, []string{"3", "3", "3"}).SetCell(2, 1, "-").
		Build()

	// Assert
	assertRenderable(t, table,
		"213",
		"2 3",
		"  3",
		"---")
}

func Test_Table_SetCellsDifferentHeightsSpan(t *testing.T) {
	// Arrange

	// Act
	table := display.NewTableBuilder().
		SetCell(0, 0, "1").SetCell(1, 0, "2").SetCell(2, 0, "3").
		SetCell(0, 1, display.Span{Content: []string{"2", "2"}, Span: 3}).
		SetCell(0, 2, "1").SetCell(1, 2, "2").SetCell(2, 2, "3").
		Build()

	// Assert
	assertRenderable(t, table,
		"123",
		"2",
		"2",
		"123")
}

func Test_Table_EmptyTile(t *testing.T) {
	// Arrange

	// Act
	table := display.NewTableBuilder().
		SetCell(0, 0, []string{"2", "2"}).SetCell(0, 1, "-").
		SetCell(1, 0, "1").SetCell(1, 1, "-").
		SetCell(2, 0, tile.NewSizedTile(xy.NewSize(1, 3), "")).SetCell(2, 1, "-").
		Build()

	// Assert
	assertRenderable(t, table,
		"21 ",
		"2  ",
		"   ",
		"---")
}

func Test_Table_AppendRow(t *testing.T) {
	// Arrange
	cell00 := []string{"2", "2"}
	cell10 := "1"
	cell01 := "three"
	cell11 := "--"

	// Act
	table := display.NewTableBuilder().
		AppendRow(cell00, cell10).
		AppendRow(cell01, cell11).
		Build()

	// Assert
	assertRenderable(t, table,
		"2    1",
		"2    ",
		"three--")
}

func Test_Table_AppendRowColor(t *testing.T) {
	// Arrange
	cell00 := []string{RED.ApplyWithResetTo("2"), "2"}
	cell10 := "1"
	cell01 := GREEN.ApplyWithResetTo("three")
	cell11 := "--"

	// Act
	table := display.NewTableBuilder().
		AppendRow(cell00, cell10).
		AppendRow(cell01, cell11).
		Build()

	// Assert
	assertRenderable(t, table,
		RED.ApplyWithResetTo("2")+"    1",
		"2    ",
		GREEN.ApplyWithResetTo("three")+"--")
}

func Test_Table_AppendRowSpan(t *testing.T) {
	// Act
	table := display.NewTableBuilder().
		AppendRow(display.Span{Content: []string{"00a", "00b"}, Span: 2}, "01", []string{"02a", "02b"}).
		AppendRow("01", "02", "03").
		Build()

	// Assert
	assertRenderable(t, table,
		"00a 0102a",
		"00b   02b",
		"010203")
}

func Test_Table_HorizontalSeparator(t *testing.T) {
	// Arrange

	// Act
	table := createTestTable().
		WithHorizontalSeparator('*').
		Build()

	// Assert
	assertRenderable(t, table,
		"abc",
		"***",
		"def",
		"***",
		"ghi")
}

func Test_Table_VerticalSeparator(t *testing.T) {
	// Arrange

	// Act
	table := createTestTable().
		WithVerticalSeparator('*').
		Build()

	// Assert
	assertRenderable(t, table,
		"a*b*c",
		"d*e*f",
		"g*h*i")
}

func Test_Table_VerticalSeparatorSpan(t *testing.T) {
	// Arrange

	// Act
	table := display.NewTableBuilder().
		AppendRow("a", "b", "c").
		AppendRow("d", display.Span{Content: "xyz", Span: 2}, "f").
		AppendRow("g", "h", "i").
		WithVerticalSeparator('|').
		Build()

	// Assert
	assertRenderable(t, table,
		"a|b|c|",
		"d|xyz|f",
		"g|h|i|")
}

func Test_Table_BothSeparators(t *testing.T) {
	// Arrange

	// Act
	table := createTestTable().
		WithHorizontalSeparator('-').
		WithVerticalSeparator('|').
		Build()

	// Assert
	assertRenderable(t, table,
		"a|b|c",
		"-----", // Favours horizontal if no intersections
		"d|e|f",
		"-----",
		"g|h|i")
}

func Test_Table_Intersections(t *testing.T) {
	// Arrange
	horizontal := '-'
	vertical := '|'
	rules := display.NewIntersectionRuleBuilder().
		AddIntersection(&vertical, &vertical, &horizontal, &horizontal, '+').
		Build()

	// Act
	table := createTestTable().
		WithHorizontalSeparator(horizontal).
		WithVerticalSeparator(vertical).
		Build()

	// Assert
	assertTile(t, table.Render(rules),
		"a|b|c",
		"-+-+-",
		"d|e|f",
		"-+-+-",
		"g|h|i")
}

func Test_Table_IntersectionsDifferentHeights(t *testing.T) {
	// Arrange
	horizontal := '-'
	vertical := '|'
	rules := display.NewIntersectionRuleBuilder().
		AddIntersection(&vertical, &vertical, &horizontal, &horizontal, '+').
		Build()

	// Act
	table := display.NewTableBuilder().
		AppendRow("a", "b", "c").
		AppendRow("d", []string{RED.ApplyWithResetTo("x"), "y"}, "f").
		AppendRow("g", "h", "i").
		WithHorizontalSeparator(horizontal).
		WithVerticalSeparator(vertical).
		Build()

	// Assert
	assertTile(t, table.Render(rules),
		"a|b|c",
		"-+-+-",
		"d|"+RED.ApplyWithResetTo("x")+"|f",
		" |y| ",
		"-+-+-",
		"g|h|i")
}

func Test_Table_IntersectionsColors(t *testing.T) {
	// Arrange
	horizontal := '-'
	vertical := '|'
	rules := display.NewIntersectionRuleBuilder().
		AddIntersection(&vertical, &vertical, &horizontal, &horizontal, '+').
		Build()

	// Act
	table := createTestTableColor().
		WithHorizontalSeparator(horizontal).
		WithVerticalSeparator(vertical).
		Build()

	// Assert
	assertTile(t, table.Render(rules),
		fmt.Sprintf("%s|b|c", RED.ApplyWithResetTo("a")),
		"-+-+-",
		fmt.Sprintf("d|%s|f", GREEN.ApplyWithResetTo("e")),
		"-+-+-",
		fmt.Sprintf("g|h|%s", BLUE.ApplyWithResetTo("i")))
}

func Test_Table_IntersectionsSpan(t *testing.T) {
	// Arrange
	horizontal := '-'
	vertical := '|'
	rules := display.NewIntersectionRuleBuilder().
		AddIntersection(&vertical, &vertical, &horizontal, &horizontal, '+').
		AddIntersection(&vertical, &vertical, &horizontal, nil, '+').
		AddIntersection(&vertical, &vertical, nil, &horizontal, '+').
		AddIntersection(&vertical, nil, &horizontal, &horizontal, '+').
		AddIntersection(nil, &vertical, &horizontal, &horizontal, '+').
		Build()

	// Act
	table := display.NewTableBuilder().
		AppendRow(RED.ApplyWithResetTo("a"), "b", "c", "d").
		AppendRow(display.Span{Content: "ef" + GREEN.ApplyWithResetTo("g") + "h", Span: 4}).
		AppendRow("i", "j", "k", BLUE.ApplyWithResetTo("l")).
		WithHorizontalSeparator(horizontal).
		WithVerticalSeparator(vertical).
		Build()

	// Assert
	assertTile(t, table.Render(rules),
		fmt.Sprintf("%s|b|c|d", RED.ApplyWithResetTo("a")),
		"-+-+-+-",
		fmt.Sprintf("ef%sh", GREEN.ApplyWithResetTo("g")),
		"-+-+-+-",
		fmt.Sprintf("i|j|k|%s", BLUE.ApplyWithResetTo("l")))
}

func Test_Table_IntersectionsUnicodeSpan(t *testing.T) {
	// Arrange
	horizontal := '─'
	vertical := '│'
	rules := rules2.UnicodeIntersections

	// Act
	table := display.NewTableBuilder().
		AppendRow(RED.ApplyWithResetTo("a"), "b", "c", "d").
		AppendRow("e", display.Span{Content: "f" + GREEN.ApplyWithResetTo("g"), Span: 2}, "h").
		AppendRow("i", "j", "k", BLUE.ApplyWithResetTo("l")).
		WithHorizontalSeparator(horizontal).
		WithVerticalSeparator(vertical).
		Build()

	// Assert
	assertTile(t, table.Render(rules),
		fmt.Sprintf("%s╷b╷c╷d", RED.ApplyWithResetTo("a")),
		"╶┼─┴─┼╴",
		fmt.Sprintf("e│f%s │h", GREEN.ApplyWithResetTo("g")),
		"╶┼─┬─┼╴",
		fmt.Sprintf("i╵j╵k╵%s", BLUE.ApplyWithResetTo("l")))
}

func Test_Table_SpanWiderThanTable(t *testing.T) {
	// Arrange
	horizontal := '─'
	vertical := '│'
	rules := rules2.UnicodeIntersections

	// Act
	table := display.NewTableBuilder().
		AppendRow(RED.ApplyWithResetTo("a"), "b", "c", "d").
		AppendRow("e", display.Span{Content: "f" + GREEN.ApplyWithResetTo("g") + "hxxxx", Span: 4}).
		AppendRow("i", "j", "k", BLUE.ApplyWithResetTo("l")).
		WithHorizontalSeparator(horizontal).
		WithVerticalSeparator(vertical).
		Build()

	// Assert
	assertTile(t, table.Render(rules),
		fmt.Sprintf("%s╷b╷c╷d╷", RED.ApplyWithResetTo("a")),
		"╶┼─┴─┴─┴╴",
		fmt.Sprintf("e│f%shxxxx", GREEN.ApplyWithResetTo("g")),
		"╶┼─┬─┬─┬╴",
		fmt.Sprintf("i╵j╵k╵%s╵", BLUE.ApplyWithResetTo("l")))
}

func Test_Table_SpanWiderThanTableContentSmaller(t *testing.T) {
	// Arrange
	horizontal := '─'
	vertical := '│'
	rules := rules2.UnicodeIntersections

	// Act
	table := display.NewTableBuilder().
		AppendRow(RED.ApplyWithResetTo("a"), "b", "c", "d").
		AppendRow("e", display.Span{Content: "f" + GREEN.ApplyWithResetTo("g") + "h", Span: 4}).
		AppendRow("i", "j", "k", BLUE.ApplyWithResetTo("l")).
		WithHorizontalSeparator(horizontal).
		WithVerticalSeparator(vertical).
		Build()

	// Assert
	assertTile(t, table.Render(rules),
		fmt.Sprintf("%s╷b╷c╷d╷", RED.ApplyWithResetTo("a")),
		"╶┼─┴─┴─┘",
		fmt.Sprintf("e│f%sh", GREEN.ApplyWithResetTo("g")),
		"╶┼─┬─┬─┐",
		fmt.Sprintf("i╵j╵k╵%s╵", BLUE.ApplyWithResetTo("l")))
}

func Test_Table_IntersectionsUnicodeSpanFull(t *testing.T) {
	// Arrange
	horizontal := '─'
	vertical := '│'
	rules := rules2.UnicodeIntersections

	// Act
	table := display.NewTableBuilder().
		AppendRow(RED.ApplyWithResetTo("a"), "b", "c", "d").
		AppendRow(display.Span{Content: "ef" + GREEN.ApplyWithResetTo("g") + "h", Span: -1}).
		AppendRow("i", "j", "k", BLUE.ApplyWithResetTo("l")).
		WithHorizontalSeparator(horizontal).
		WithVerticalSeparator(vertical).
		Build()

	// Assert
	assertTile(t, table.Render(rules),
		fmt.Sprintf("%s╷b╷c╷d", RED.ApplyWithResetTo("a")),
		"╶┴─┴─┴╴",
		fmt.Sprintf("ef%sh", GREEN.ApplyWithResetTo("g")),
		"╶┬─┬─┬╴",
		fmt.Sprintf("i╵j╵k╵%s", BLUE.ApplyWithResetTo("l")))
}

func Test_Table_Misc1(t *testing.T) {
	// Arrange
	table := display.NewTableBuilder().WithVerticalSeparator(' ').
		AppendRow("Label", "Addr", "Used at location").
		AppendRow("-----", "----", "----------------").
		AppendRow("START", "0000").
		AppendRow("ERROR_1", "0008").
		AppendRow("PRINT_A_1", "0010").
		AppendRow("GET_CHAR", "0018").
		AppendRow("TEST_CHAR", "001c", "[0023]").
		AppendRow("NEXT_CHAR", "0020").
		AppendRow("FP_CALC", "0028").
		AppendRow("ERROR_2", "0053", "[000e]").
		AppendRow("CH_ADD_1", "0074", "[0020]").
		AppendRow("SKIP_OVER", "007d", "[001c]").
		AppendRow("START_NEW", "11cb", "[0005]").
		AppendRow("PRINT_A_2", "15f2", "[0010]").
		AppendRow("CALCULATE", "335b", "[0028]").
		AppendRow("CH_ADD", "5c5d", "[0008,0018]").
		AppendRow("X_PTR", "5c5f", "[000b]").
		Build()

	// Act
	result := table.Render()

	// Assert
	assertTile(t, result,
		"Label     Addr Used at location",
		"-----     ---- ----------------",
		"START     0000",
		"ERROR_1   0008",
		"PRINT_A_1 0010",
		"GET_CHAR  0018",
		"TEST_CHAR 001c [0023]",
		"NEXT_CHAR 0020",
		"FP_CALC   0028",
		"ERROR_2   0053 [000e]",
		"CH_ADD_1  0074 [0020]",
		"SKIP_OVER 007d [001c]",
		"START_NEW 11cb [0005]",
		"PRINT_A_2 15f2 [0010]",
		"CALCULATE 335b [0028]",
		"CH_ADD    5c5d [0008,0018]",
		"X_PTR     5c5f [000b]",
	)
}

func createTestTable() display.TableBuilder {
	return display.NewTableBuilder().
		AppendRow("a", "b", "c").
		AppendRow("d", "e", "f").
		AppendRow("g", "h", "i")
}

func createTestTableColor() display.TableBuilder {
	return display.NewTableBuilder().
		AppendRow(RED.ApplyWithResetTo("a"), "b", "c").
		AppendRow("d", GREEN.ApplyWithResetTo("e"), "f").
		AppendRow("g", "h", BLUE.ApplyWithResetTo("i"))
}
