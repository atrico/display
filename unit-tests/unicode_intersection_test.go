package unit_tests

import (
	"gitlab.com/atrico/display/v2"
	"testing"

	"gitlab.com/atrico/console/box_drawing"

	"gitlab.com/atrico/display/v2/rules"
	"gitlab.com/atrico/display/v2/tile"
	"gitlab.com/atrico/display/v2/xy"
)

func Test_UnicodeIntersections_Single(t *testing.T) {
	// Arrange

	// Act
	hor := box_drawing.GetHorizontal(box_drawing.BoxSingle)
	vert := box_drawing.GetVertical(box_drawing.BoxSingle)
	square := createSquare(hor, hor, hor, vert, vert, vert)

	// Assert
	assertTile(t, square.Render(rules.UnicodeIntersections),
		"┌─┬─┐",
		"│ │ │",
		"├─┼─┤",
		"│ │ │",
		"└─┴─┘")
}

func Test_UnicodeIntersections_Double(t *testing.T) {
	// Arrange

	// Act
	hor := box_drawing.GetHorizontal(box_drawing.BoxDouble)
	vert := box_drawing.GetVertical(box_drawing.BoxDouble)
	square := createSquare(hor, hor, hor, vert, vert, vert)

	// Assert
	assertTile(t, square.Render(rules.UnicodeIntersections),
		"╔═╦═╗",
		"║ ║ ║",
		"╠═╬═╣",
		"║ ║ ║",
		"╚═╩═╝")
}

func Test_UnicodeIntersections_Heavy(t *testing.T) {
	// Arrange

	// Act
	hor := box_drawing.GetHorizontal(box_drawing.BoxHeavy)
	vert := box_drawing.GetVertical(box_drawing.BoxHeavy)
	square := createSquare(hor, hor, hor, vert, vert, vert)

	// Assert
	assertTile(t, square.Render(rules.UnicodeIntersections),
		"┏━┳━┓",
		"┃ ┃ ┃",
		"┣━╋━┫",
		"┃ ┃ ┃",
		"┗━┻━┛")
}

func Test_UnicodeIntersections_DoubleSingle(t *testing.T) {
	// Arrange

	// Act
	hor := box_drawing.GetHorizontal(box_drawing.BoxDouble)
	vert := box_drawing.GetVertical(box_drawing.BoxSingle)
	square := createSquare(hor, hor, hor, vert, vert, vert)

	// Assert
	assertTile(t, square.Render(rules.UnicodeIntersections),
		"╒═╤═╕",
		"│ │ │",
		"╞═╪═╡",
		"│ │ │",
		"╘═╧═╛")
}

func Test_UnicodeIntersections_SingleDouble(t *testing.T) {
	// Arrange

	// Act
	hor := box_drawing.GetHorizontal(box_drawing.BoxSingle)
	vert := box_drawing.GetVertical(box_drawing.BoxDouble)
	square := createSquare(hor, hor, hor, vert, vert, vert)

	// Assert
	assertTile(t, square.Render(rules.UnicodeIntersections),
		"╓─╥─╖",
		"║ ║ ║",
		"╟─╫─╢",
		"║ ║ ║",
		"╙─╨─╜")
}

func Test_UnicodeIntersections_HeavySingle(t *testing.T) {
	// Arrange

	// Act
	hor := box_drawing.GetHorizontal(box_drawing.BoxHeavy)
	vert := box_drawing.GetVertical(box_drawing.BoxSingle)
	square := createSquare(hor, hor, hor, vert, vert, vert)

	// Assert
	assertTile(t, square.Render(rules.UnicodeIntersections),
		"┍━┯━┑",
		"│ │ │",
		"┝━┿━┥",
		"│ │ │",
		"┕━┷━┙")
}

func Test_UnicodeIntersections_SingleHeavy(t *testing.T) {
	// Arrange

	// Act
	hor := box_drawing.GetHorizontal(box_drawing.BoxSingle)
	vert := box_drawing.GetVertical(box_drawing.BoxHeavy)
	square := createSquare(hor, hor, hor, vert, vert, vert)

	// Assert
	assertTile(t, square.Render(rules.UnicodeIntersections),
		"┎─┰─┒",
		"┃ ┃ ┃",
		"┠─╂─┨",
		"┃ ┃ ┃",
		"┖─┸─┚")
}

func Test_UnicodeIntersections_LineEndsSingleChar(t *testing.T) {
	// Arrange

	// Act
	lineS := display.NewHorizontalLine(box_drawing.GetHorizontal(box_drawing.BoxSingle), 1)
	lineD := display.NewHorizontalLine(box_drawing.GetHorizontal(box_drawing.BoxDouble), 1)
	lineH := display.NewHorizontalLine(box_drawing.GetHorizontal(box_drawing.BoxHeavy), 1)
	colS := display.NewVerticalLine(box_drawing.GetVertical(box_drawing.BoxSingle), 1)
	colD := display.NewVerticalLine(box_drawing.GetVertical(box_drawing.BoxDouble), 1)
	colH := display.NewVerticalLine(box_drawing.GetVertical(box_drawing.BoxHeavy), 1)
	canvasH := display.NewCanvas().
		WriteAt(xy.NewPosition(0, 0), lineS).
		WriteAt(xy.NewPosition(0, 1), lineD).
		WriteAt(xy.NewPosition(0, 2), lineH)
	canvasV := display.NewCanvas().
		WriteAt(xy.NewPosition(0, 0), colS).
		WriteAt(xy.NewPosition(1, 0), colD).
		WriteAt(xy.NewPosition(2, 0), colH)

	// Assert
	assertTile(t, canvasH.Render(rules.UnicodeIntersections), "─", "═", "━")
	assertTile(t, canvasV.Render(rules.UnicodeIntersections), "│║┃")
}

func Test_UnicodeIntersections_LineEnds(t *testing.T) {
	// Arrange

	// Act
	lineS := display.NewHorizontalLine(box_drawing.GetHorizontal(box_drawing.BoxSingle), 3)
	lineD := display.NewHorizontalLine(box_drawing.GetHorizontal(box_drawing.BoxDouble), 3)
	lineH := display.NewHorizontalLine(box_drawing.GetHorizontal(box_drawing.BoxHeavy), 3)
	colS := display.NewVerticalLine(box_drawing.GetVertical(box_drawing.BoxSingle), 3)
	colD := display.NewVerticalLine(box_drawing.GetVertical(box_drawing.BoxDouble), 3)
	colH := display.NewVerticalLine(box_drawing.GetVertical(box_drawing.BoxHeavy), 3)
	canvasH := display.NewCanvas().
		WriteAt(xy.NewPosition(0, 0), lineS).
		WriteAt(xy.NewPosition(0, 1), lineD).
		WriteAt(xy.NewPosition(0, 2), lineH)
	canvasV := display.NewCanvas().
		WriteAt(xy.NewPosition(0, 0), colS).
		WriteAt(xy.NewPosition(1, 0), colD).
		WriteAt(xy.NewPosition(2, 0), colH)

	// Assert
	assertTile(t, canvasH.Render(rules.UnicodeIntersections), "╶─╴", "═══", "╺━╸")
	assertTile(t, canvasV.Render(rules.UnicodeIntersections), "╷║╻", "│║┃", "╵║╹")
}

func createSquare(top, equator, bottom rune, left, middle, right rune) tile.Renderable {
	horLineT := display.NewHorizontalLine(top, 3)
	horLineE := display.NewHorizontalLine(equator, 3)
	horLineB := display.NewHorizontalLine(bottom, 3)
	vertLineL := display.NewVerticalLine(left, 3)
	vertLineM := display.NewVerticalLine(middle, 3)
	vertLineR := display.NewVerticalLine(right, 3)
	return display.NewCanvas().
		WriteAt(xy.NewPosition(1, 0), horLineT).
		WriteAt(xy.NewPosition(1, 2), horLineE).
		WriteAt(xy.NewPosition(1, 4), horLineB).
		WriteAt(xy.NewPosition(0, 1), vertLineL).
		WriteAt(xy.NewPosition(2, 1), vertLineM).
		WriteAt(xy.NewPosition(4, 1), vertLineR)
}
