package unit_tests

import (
	"fmt"
	"strings"
	"testing"

	"gitlab.com/atrico/console/ansi"
	"gitlab.com/atrico/console/ansi/color"
	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/is"

	"gitlab.com/atrico/display/v2/common"
)

func Test_Helpers_MaxWidth(t *testing.T) {
	// Arrange
	lines := make([]string, 3)
	lines[0] = strings.Repeat("a", 2)
	lines[1] = strings.Repeat("b", 8)
	lines[2] = strings.Repeat("c", 3)

	// Act
	max := common.GetMaxWidth(lines)

	// Assert
	assert.That(t, max, is.EqualTo(8), "Max width")
}

func Test_Helpers_StringWidthNormal(t *testing.T) {
	// Arrange
	str := anyValue.String()
	fmt.Println(str)

	// Act
	length := common.StringWidth(str)

	// Assert
	assert.That(t, length, is.EqualTo(len(str)), "Correct length")
}

func Test_Helpers_StringWidthUnicode(t *testing.T) {
	// Arrange
	exp := 5
	runes := make([]rune, exp)
	for i := 0; i < exp; i++ {
		runes[i] = rune(anyValue.IntBetween(0x2500, 0x2580))
	}
	str := string(runes)
	fmt.Println(str)

	// Act
	length := common.StringWidth(str)

	// Assert
	assert.That(t, length, is.EqualTo(exp), "Correct length")
}

func Test_Helpers_StringWidthColor(t *testing.T) {
	// Arrange
	raw := anyValue.String()
	attr := ansi.NewAttributesFore(color.Red).SetThis()
	str := attr.ApplyTo(raw)
	fmt.Println(str)

	// Act
	length := common.StringWidth(str)

	// Assert
	assert.That(t, length, is.EqualTo(len(raw)), "Correct length")
}
