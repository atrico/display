package unit_tests

import (
	"fmt"
	"gitlab.com/atrico/console/ansi"
	"gitlab.com/atrico/console/ansi/color"
	"gitlab.com/atrico/core/v2"
	"slices"
	"strings"
	"testing"

	"gitlab.com/atrico/display/v2/common"
	"gitlab.com/atrico/display/v2/tile"
	"gitlab.com/atrico/testing/v2/assert"

	"gitlab.com/atrico/testing/v2/is"
	"gitlab.com/atrico/testing/v2/random"

	"gitlab.com/atrico/display/v2/xy"
)

var anyValue = random.NewValueGenerator()

var RED = ansi.NewAttributesFore(color.Red)
var GREEN = ansi.NewAttributesFore(color.Green)
var BLUE = ansi.NewAttributesFore(color.Blue)

func assertTile(t *testing.T, element tile.Tile, lines ...string) {
	core.DisplayMultiline(element)
	fmt.Println("Expected...")
	for _, line := range lines {
		fmt.Println(line)
	}
	width := common.GetMaxWidth(lines)
	// Size
	size := xy.NewSize(width, len(lines))
	assert.That(t, element.Size(), is.EqualTo(size), "Size")
	paddedLines := padLines(lines, width)
	// Cells
	for y := 0; y < size.Y(); y++ {
		expectedLine := ansi.StripAttributes(paddedLines[y])
		for x := 0; x < size.X(); x++ {
			pos := xy.NewPosition(x, y)
			cell, ok := element.GetCell(pos)
			assert.That(t, ok, is.True, "Cell %v exists", pos)
			assert.That(t, string(cell.Char()), is.EqualTo(string([]rune(expectedLine)[x])), "Cell %v contents", pos)
		}
	}
	// StringerMl
	mlString := slices.Collect(element.StringMl())
	assert.That(t, len(mlString), is.EqualTo(size.Y()), "Lines")
	for i := 0; i < size.Y(); i++ {
		assert.That(t, mlString[i], is.EqualTo(paddedLines[i]), "Line %d", i)
	}
	// Renderable
	rendered := element.Render()
	assert.That(t, rendered, is.DeepEqualTo(element), "Renders to same")
}

func assertRenderable(t *testing.T, renderable tile.Renderable, lines ...string) {
	assertTile(t, renderable.Render(), lines...)
}

func padLines(lines []string, width int) []string {
	paddedLines := make([]string, len(lines))
	for i, line := range lines {
		paddedLines[i] = fmt.Sprintf("%s%s", line, strings.Repeat(" ", width-common.StringWidth(line)))
	}
	return paddedLines
}

func createTestElement(size xy.Size) (element tile.Tile, lines []string) {
	lines = make([]string, size.Y())
	for y := range lines {
		line := strings.Builder{}
		for x := 0; x < size.X(); x++ {
			line.WriteString(anyValue.StringOfLen(1))
		}
		lines[y] = line.String()
	}
	return tile.NewTile(lines), lines
}

func randomColour() color.Color {
	val := anyValue.IntBetween(30, 38)
	if anyValue.Bool() {
		// Bright
		val = val + 60
	}
	return color.Color(val)
}
