package tile

import (
	"fmt"
	"slices"
	"strings"

	"gitlab.com/atrico/console/ansi"

	"gitlab.com/atrico/display/v2/cells"
	"gitlab.com/atrico/display/v2/common"

	. "gitlab.com/atrico/core/v2"

	"gitlab.com/atrico/display/v2/xy"
)

// An static displayable rectangle
type Tile interface {
	StringerMl
	// Size in characters
	Size() xy.Size
	// Get contents of a specific cell
	GetCell(position xy.Position) (cell cells.Cell, ok bool)
	// Tile is intrinsically renderable (generally return this)
	Render(rules ...RenderRule) Tile
	// Split this tile into columns
	// There will be one output tile for each width specified
	// If the source is too narrow to fulfil all widths, empty cols will be filled with an empty tile
	// If the source is too wide, the last output will be bigger than specified
	// status contains the differene in widdths, 0 = fits exactly, <0 source was too narrow, >0 source was too wide
	SplitX(widths ...int) (tiles []Tile, status int)
}

// Constructor
func NewTile(content any) Tile {
	// Cast
	switch val := content.(type) {
	case Tile:
		return val
	case Renderable:
		return val.Render()
	}

	var cellz cells.Cells = nil
	var lines []string = nil
	switch val := content.(type) {
	case cells.Cells:
		cellz = val
	case []string:
		lines = val
	case []any:
		for _, v := range val {
			lines = append(lines, fmt.Sprintf("%v", v))
		}
	case fmt.Stringer:
		lines = []string{val.String()}
	case StringerMl:
		lines = slices.Collect(val.StringMl())
	default:
		lines = []string{fmt.Sprintf("%v", val)}
	}
	if lines != nil {
		cellz = make(cells.Cells)
		pos := xy.Origin
		for _, line := range lines {
			stringToCells(&cellz, line, pos)
			pos = pos.Down(1)
		}
	}
	return &tile{cellz, nil}
}

// Tile with minimum size
func NewSizedTile(size xy.Size, content any) Tile {
	temporary := NewTile(content)
	if temporary.Size() == size {
		return temporary
	}
	lines := slices.Collect(temporary.StringMl())
	lineCount := len(lines)
	// Pad/clip height
	if lineCount < size.Y() {
		lines = append(lines, make([]string, size.Y()-lineCount)...)
	} else if lineCount > size.Y() {
		lines = lines[:size.Y()]
	}
	// Pad/clip width
	lines = padOrClipLines(lines, size.X())
	return NewTile(lines)
}

// ----------------------------------------------------------------------------------------------------------------------------
// internal
// ----------------------------------------------------------------------------------------------------------------------------

func stringToCells(cellz *cells.Cells, str string, origin xy.Position) {
	current := xy.NewPosition(origin.X(), origin.Y())
	for _, part := range ansi.ParseString(str) {
		for _, char := range []rune(part.String) {
			(*cellz)[current] = cells.NewCell(char, part.Attributes)
			current = current.Right(1)
		}
	}
}

func padOrClipLines(lines []string, width int) []string {
	newLines := make([]string, len(lines))
	for i, line := range lines {
		strWidth := common.StringWidth(line)
		if width > strWidth {
			newLines[i] = fmt.Sprintf("%s%s", line, strings.Repeat(" ", width-strWidth))
		} else if strWidth > width {
			newLines[i] = line[:width]
		} else {
			newLines[i] = line
		}
	}
	return newLines
}
