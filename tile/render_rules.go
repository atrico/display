package tile

import "gitlab.com/atrico/display/v2/cells"

type RenderRule interface {
	// Process cells with this rule
	Process(cells cells.Cells) cells.Cells
}
