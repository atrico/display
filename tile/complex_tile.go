package tile

import (
	"gitlab.com/atrico/console/ansi"
	"iter"

	"gitlab.com/atrico/display/v2/cells"
	"gitlab.com/atrico/display/v2/xy"
)

// ----------------------------------------------------------------------------------------------------------------------------
// Implementation
// ----------------------------------------------------------------------------------------------------------------------------

// Tile with "proper" cells
type tile struct {
	cells cells.Cells
	size  *xy.Size
}

func (c *tile) Size() xy.Size {
	c.lazyEvaluate()
	return *c.size
}

func (c *tile) GetCell(position xy.Position) (cell cells.Cell, ok bool) {
	c.lazyEvaluate()
	cell, ok = c.cells[position]
	return cell, ok
}

func (c *tile) StringMl(_ ...any) iter.Seq[string] {
	return func(yield func(string) bool) {
		c.lazyEvaluate()
		for _, str := range cells.ExpandCells(c.cells, xy.Origin, *c.size) {
			if !yield(str) {
				break
			}
		}
	}
}

// Renderable (Tile is static so ignore rules)
func (c *tile) Render(_ ...RenderRule) Tile {
	return c
}

func (c *tile) lazyEvaluate() {
	if c.size == nil {
		origin, size := cells.GetOriginAndSize(c.cells)
		newCells := make(cells.Cells, size.X()*size.Y())
		// Pad missing cells and re-origin to 0,0
		for _, pos := range xy.AllPositions(origin, size) {
			newPos := pos.Offset(-origin.X(), -origin.Y())
			if cell, ok := c.cells[pos]; ok {
				newCells[newPos] = cell
			} else {
				newCells[newPos] = cells.NewCell(' ', ansi.NoAttributes)
			}
		}
		c.cells = newCells
		c.size = &size
	}
}

func (c *tile) SplitX(widths ...int) (tiles []Tile, status int) {
	c.lazyEvaluate()
	tiles = make([]Tile, len(widths))
	// Create empty cells
	cellz := make([]cells.Cells, len(widths))
	for i := range cellz {
		cellz[i] = make(cells.Cells)
	}
	for pos, cell := range c.cells {
		for i, width := range widths {
			if pos.X() < width || i == len(widths)-1 {
				cellz[i][pos] = cell
				break
			}
			pos = pos.Left(width)
		}
	}
	for i, cs := range cellz {
		tiles[i] = &tile{cells: cs}
	}
	status = tiles[len(tiles)-1].Size().X() - widths[len(tiles)-1]
	return
}
