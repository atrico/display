package display

import (
	"gitlab.com/atrico/core/v2"
	"gitlab.com/atrico/display/v2/cells"
	"gitlab.com/atrico/display/v2/tile"
	"gitlab.com/atrico/display/v2/xy"
	"iter"
)

// Mutable display type
type Canvas interface {
	core.StringerMl
	// Modify
	WriteAt(position xy.Position, content any) Canvas
	OverwriteAt(position xy.Position, content any) Canvas
	// Cells
	GetCell(position xy.Position) (cell cells.Cell, ok bool)
	SetCell(position xy.Position, cell cells.Cell)
	// Renderable
	Render(rules ...tile.RenderRule) tile.Tile
}

func (c *canvas) StringMl(params ...any) iter.Seq[string] {
	return c.Render().StringMl(params...)
}

func NewCanvas() Canvas {
	return &canvas{cells: make(map[xy.Position]cells.Cell)}
}

// ----------------------------------------------------------------------------------------------------------------------------
// Implementation
// ----------------------------------------------------------------------------------------------------------------------------

type canvas struct {
	cells cells.Cells
}

// Modify
func (c *canvas) WriteAt(position xy.Position, content any) Canvas {
	return c.writeAt(position, content, true)
}

func (c *canvas) OverwriteAt(position xy.Position, content any) Canvas {
	return c.writeAt(position, content, false)
}

func (c *canvas) writeAt(position xy.Position, content any, transparency bool) Canvas {
	// Create static tile
	theTile := tile.NewTile(content)
	for _, pos := range xy.AllPositions(xy.Origin, theTile.Size()) {
		if cell, ok := theTile.GetCell(pos); ok {
			c.setCell(position.Offset(pos.X(), pos.Y()), cell, transparency)
		}
	}
	return c
}

// Cells
func (c *canvas) GetCell(position xy.Position) (cell cells.Cell, ok bool) {
	cell, ok = c.cells[position]
	return cell, ok
}

func (c *canvas) SetCell(position xy.Position, cell cells.Cell) {
	c.cells[position] = cell
}

// Renderable
func (c *canvas) Render(rules ...tile.RenderRule) tile.Tile {
	theCells := c.cells
	for _, rule := range rules {
		theCells = rule.Process(theCells)
	}
	// origin, size := cells.GetOriginAndSize(theCells)
	// return tile.NewTile(cells.ExpandCells(theCells, origin, size))
	return tile.NewTile(theCells)
}

func (c *canvas) setCell(position xy.Position, cell cells.Cell, transparency bool) {
	// Write new cell
	if cell.Char() != ' ' || !transparency {
		// If not transparent, overwrite
		c.cells[position] = cell
	} else {
		// If transparent, only write if nothing already there (to keep size correct)
		if _, ok := c.cells[position]; !ok {
			c.cells[position] = cell
		}
	}
	c.StringMl()
}
