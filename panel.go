package display

import (
	"gitlab.com/atrico/display/v2/tile"
	"gitlab.com/atrico/display/v2/xy"
)

type PositionedRenderable struct {
	Position xy.Position
	tile.Renderable
}

// Constructors
func NewPanel(content ...PositionedRenderable) tile.Renderable {
	return panel{content}
}

// ----------------------------------------------------------------------------------------------------------------------------
// Implementation
// ----------------------------------------------------------------------------------------------------------------------------

type panel struct {
	children []PositionedRenderable
}

// Renderable
func (p panel) Render(rules ...tile.RenderRule) tile.Tile {
	canvas := NewCanvas()
	for _, child := range p.children {
		canvas.WriteAt(child.Position, child.Render())
	}
	return canvas.Render(rules...)
}
