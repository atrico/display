//go:build ignore

package main

import (
	"gitlab.com/atrico/console/box_drawing"
	"gitlab.com/atrico/core/v2"
	"gitlab.com/atrico/display/v2"
	"gitlab.com/atrico/display/v2/rules"
)

func main() {
	// Numbers
	numbers := make([][]int, 9)
	for i := range 9 {
		numbers[i] = []int{1, 2, 3, 4, 5, 6, 7, 8, 9}
	}
	// Board
	board := display.NewTableBuilder().
		WithHorizontalSeparator(box_drawing.GetHorizontal(box_drawing.BoxDouble)).
		WithVerticalSeparator(box_drawing.GetVertical(box_drawing.BoxDouble))
	for y := range 3 {
		for x := range 3 {
			square := display.NewTableBuilder().
				WithHorizontalSeparator(box_drawing.GetHorizontal(box_drawing.BoxSingle)).
				WithVerticalSeparator(box_drawing.GetVertical(box_drawing.BoxSingle))
			for y2 := range 3 {
				for x2 := range 3 {
					square.SetCell(x2, y2, numbers[y*3+y2][x*3+x2])
				}
			}
			board.SetCell(x, y, square)
		}
	}
	horizontal := box_drawing.GetHorizontal(box_drawing.BoxDouble)
	vertical := box_drawing.GetVertical(box_drawing.BoxDouble)
	border := display.NewBorder(board.Build(), &horizontal, &horizontal, &vertical, &vertical)
	// Display
	core.DisplayMultiline(border.Render(rules.UnicodeIntersections))
}
