//go:build ignore

package main

import (
	"gitlab.com/atrico/console/ansi"
	"gitlab.com/atrico/console/ansi/color"
	"gitlab.com/atrico/console/box_drawing"
	"gitlab.com/atrico/display/v2/rules"
	"gitlab.com/atrico/display/v2/tile"
	"gitlab.com/atrico/display/v2/xy"
)

// Size of cube
const size = 5

func main() {
	front := createFace(color.Blue, size)
	back := createFace(color.Green, size)
	top := createFace(color.White, size)
	bottom := createFace(color.Yellow, size)
	left := createFace(color.Red, size)
	orange := ansi.NewAttributes(color.Red, color.Yellow)
	right := createFaceImpl(orange, "\u2592", size) // Orange
	size := front.Render().Size().Add(xy.NewSize(-1, -1))
	cube := display.NewPanel(
		display.PositionedRenderable{Position: xy.NewPosition(0, 0), Renderable: front},
		display.PositionedRenderable{Position: xy.NewPosition(0, -size.Y()), Renderable: top},
		display.PositionedRenderable{Position: xy.NewPosition(0, +size.Y()), Renderable: bottom},
		display.PositionedRenderable{Position: xy.NewPosition(-size.X(), 0), Renderable: left},
		display.PositionedRenderable{Position: xy.NewPosition(+size.X(), 0), Renderable: right},
		display.PositionedRenderable{Position: xy.NewPosition(+2*size.X(), 0), Renderable: back}).
		Render(rules.UnicodeIntersections)
	core.DisplayMultiline(cube)
}

func createFace(col color.Color, size int) tile.Renderable {
	attr := ansi.NewAttributesBack(col)
	return createFaceImpl(attr, " ", size)
}

func createFaceImpl(attr ansi.Attributes, char string, size int) tile.Renderable {
	t := attr.SetThis().ApplyTo(char)
	table := display.NewTableBuilder().
		WithHorizontalSeparator(box_drawing.GetHorizontal(box_drawing.BoxSingle)).
		WithVerticalSeparator(box_drawing.GetVertical(box_drawing.BoxSingle))
	for y := 0; y < size; y++ {
		for x := 0; x < size; x++ {
			table.SetCell(x, y, t)
		}
	}
	borderH := box_drawing.GetHorizontal(box_drawing.BoxDouble)
	borderV := box_drawing.GetVertical(box_drawing.BoxDouble)
	return display.NewBorder(table.Build(), &borderH, &borderH, &borderV, &borderV)
}
