package rules

import (
	"gitlab.com/atrico/console/ansi"
	"gitlab.com/atrico/console/box_drawing"

	"gitlab.com/atrico/display/v2/cells"
	"gitlab.com/atrico/display/v2/tile"
	"gitlab.com/atrico/display/v2/xy"
)

var UnicodeIntersections tile.RenderRule = unicodeIntersections{}

// ----------------------------------------------------------------------------------------------------------------------------
// Implementation
// ----------------------------------------------------------------------------------------------------------------------------

type unicodeIntersections struct{}

func (u unicodeIntersections) Process(cellz cells.Cells) cells.Cells {
	newCells := make(cells.Cells, len(cellz))
	origin, size := cells.GetOriginAndSize(cellz)
	// Process intersections
	for _, pos := range xy.AllPositions(origin, size) {
		matchCount := 0
		requiredChar := box_drawing.BoxParts{}
		checkAdjacentCell(cellz, pos, up, &requiredChar, &matchCount)
		checkAdjacentCell(cellz, pos, down, &requiredChar, &matchCount)
		checkAdjacentCell(cellz, pos, left, &requiredChar, &matchCount)
		checkAdjacentCell(cellz, pos, right, &requiredChar, &matchCount)
		if newChar, ok := box_drawing.GetBoxCharMixed(requiredChar); ok && matchCount > 1 {
			newCells[pos] = cells.NewCell(newChar, ansi.NoAttributes, cells.Line)
		} else if old, ok := cellz[pos]; ok {
			newCells[pos] = old
		}
	}
	// Process line ends
	newCells2 := make(cells.Cells, len(newCells))
	for _, pos := range xy.AllPositions(origin, size) {
		if old, ok := newCells[pos]; ok {
			replaced := false
			linePart := box_drawing.BoxNone
			horizontal := false
			// Only process vertical or horizontal lines
			if old.Flags().HasFlag(cells.Line) {
				switch old.Char() {
				case box_drawing.GetHorizontal(box_drawing.BoxSingle):
					linePart = box_drawing.BoxSingle
					horizontal = true
				case box_drawing.GetHorizontal(box_drawing.BoxHeavy):
					linePart = box_drawing.BoxHeavy
					horizontal = true
				case box_drawing.GetVertical(box_drawing.BoxSingle):
					linePart = box_drawing.BoxSingle
				case box_drawing.GetVertical(box_drawing.BoxHeavy):
					linePart = box_drawing.BoxHeavy
				}
				if linePart != box_drawing.BoxNone {
					matchCount := 0
					requiredChar := box_drawing.BoxParts{}
					if horizontal {
						checkAdjacentCell(newCells, pos, left, &requiredChar, &matchCount)
						checkAdjacentCell(newCells, pos, right, &requiredChar, &matchCount)
					} else {
						checkAdjacentCell(newCells, pos, up, &requiredChar, &matchCount)
						checkAdjacentCell(newCells, pos, down, &requiredChar, &matchCount)
					}
					if matchCount == 1 {
						if newChar, ok := box_drawing.GetBoxCharMixed(requiredChar); ok {
							newCells2[pos] = cells.NewCell(newChar, ansi.NoAttributes, cells.Line)
							replaced = true
						}
					}
				}
			}
			if !replaced {
				newCells2[pos] = old
			}
		}
	}
	return newCells2
}

type dir int

const (
	up    dir = iota
	down  dir = iota
	left  dir = iota
	right dir = iota
)

func checkAdjacentCell(cellz cells.Cells, pos xy.Position, dir dir, requiredChar *box_drawing.BoxParts, matchCount *int) {
	cell, ok := cellz[getPosOffset(pos, dir)]
	if ok && cell.Flags().HasFlag(cells.Line) {
		if lineChar, ok := box_drawing.Lookup(cell.Char()); ok {
			setRequiredChar(requiredChar, matchCount, lineChar, dir)
		}
	}
}

func getPosOffset(position xy.Position, dir dir) xy.Position {
	switch dir {
	case up:
		return position.Up(1)
	case down:
		return position.Down(1)
	case left:
		return position.Left(1)
	case right:
		return position.Right(1)
	}
	panic("Invalid dir value")
}

func setRequiredChar(requiredChar *box_drawing.BoxParts, matchCount *int, parts box_drawing.BoxParts, dir dir) {
	switch dir {
	case up:
		if parts.Down != box_drawing.BoxNone {
			requiredChar.Up = parts.Down
			*matchCount++
		}
	case down:
		if parts.Up != box_drawing.BoxNone {
			requiredChar.Down = parts.Up
			*matchCount++
		}
	case left:
		if parts.Right != box_drawing.BoxNone {
			requiredChar.Left = parts.Right
			*matchCount++
		}
	case right:
		if parts.Left != box_drawing.BoxNone {
			requiredChar.Right = parts.Left
			*matchCount++
		}
	}
}
