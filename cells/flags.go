package cells

import (
	"strings"
)

type Flag int
type Flags int

const (
	Line Flag = 0b1
)

func (f Flag) String() string {
	switch f {
	case Line:
		return "Line"
	}
	return "Unknown"
}

func (f Flags) String() string {
	text := strings.Builder{}
	text.WriteString("[")
	if f.HasFlag(Line) {
		text.WriteString("Line")
	}
	text.WriteString("]")
	return text.String()
}

func NewFlags(flags ...Flag) Flags {
	newFlags := Flags(0)
	for _, flag := range flags {
		newFlags = newFlags.Add(flag)
	}
	return newFlags
}

func (f Flags) Add(flag Flag) Flags {
	return Flags(int(f) | int(flag))
}

func (f Flags) HasFlag(flag Flag) bool {
	return int(f)&int(flag) != 0
}
